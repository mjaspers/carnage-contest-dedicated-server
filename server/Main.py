'''
Created on Dec 23, 2012

@author: Gebruiker
'''

import socket,atexit,threading,select
#from bb_udp import *
from time import *

from StreamCC import *
from entities import *
from collections import deque

sv_port = 36960
sv_name = "Uprate6 Test Server"
sv_currentplayers = 1
sv_maxplayers = 8
sv_gamemode = 0
sv_player_health = 100
sv_turn_time = 45
sv_backing_time = 3
sv_game_ender_start = 4
sv_game_ender_type = 0
sv_crates_on_map_limit = 15
sv_supply_traps = 10
sv_supply_crates = 3
sv_medikits = 1
sv_mines = 1
sv_mine_duds = 10
sv_explosion_delay = 3
sv_explosion_damage = 3
sv_bear_traps = 1
sv_fall_damage = 0
sv_friction = 2
sv_wind = 2

sv_map_x = 1300
sv_map_y = 600

sv_medikits = 1
sv_medikit_healing = 25

admin_play_name = "uprate6"
admin_play_team = "team ronflonflon"

sends = deque()
players = []


class Serv(threading.Thread):
    def __init__(self, infile, addr):
        threading.Thread.__init__(self)        
        self.infile = infile
        self.addr = addr
    def run(self):
        data = self.infile
        handler(data, self.addr)

def start():
    global usgn
    global ip
    global running
    global server
    
    server = socket.socket(socket.AF_INET,socket.SOCK_DGRAM)
    #server.setblocking(0)
    
    try:
        server.bind(('',sv_port))
        running = 1
        add("Server has been started")        
    except socket.error,err:
        running = 0
        add("Couldn't be a udp server on port %d : %s" %(sv_port,err))
        return 0
    register()
    
    admin_player = Player(admin_play_name,'',('127.0.0.1',36963))
    admin_player.set_team(admin_play_team)
    players.append(admin_player)
    admin_player.id = 0
        
#register server with USGN
def register(): 
    global usgn_address
    usgn_address = socket.gethostbyname("www.usgn.de") # get usgn ip
    data =WriteStreamCC() # create new stream
    data.WriteShort(1);data.WriteByte(0x21);data.WriteByte(0x02);
    data.WriteString(sv_name)    
    sends.append((data.get(),(usgn_address,36963))) #append stream to send queue   
    
    send_heartbeat()  


def add(string):
    string = "[" + strftime("%H:%M:%S",localtime())+ "]"+ string
    print string

def run():
    while running == 1:
        frametime = time()
        (read,write,error) = select.select([server],[server],[server])
        if server in read:
            try:
                data,addr = server.recvfrom(1024)
            except:
                add("Error during receiving data!")                
        
            try:
                rs = ReadStreamCC(data)
                handle =  Serv(rs,addr)
                handle.start()
            except KeyboardInterrupt:
                raise "KeyboardInterrupt"
            except:
                add("Error during handling data!")
                
        update(frametime)       
            
        if server in write:
            send()
        frametime = (time() - frametime)
        sleep(0.02)
        
    server.close()
    add("Server has been closed")

def sendone(data,addr,imp =False,unknown = False):    
        if unknown==False:
            pl= get_player(addr)
            pnm = pl.pktnum
            if imp == True:            
                data = header(pnm) + data
                pl.pktnum += 2
            else:
                data = header(pnm - 1) + data    
        else:
            data = header(1) + data      
            
            
        #if time_ == 0:
         #   time_ = time()
        sends.append((data,addr))# time_))
        
def sendall(data,imp = False):
    for pl in players:
        sendone(data,pl.address,imp)
    

def send():
    for i in range(0,len(sends)):
        data, addr = sends.popleft()
        server.sendto(data,addr)   
        
    
    #for (data, addr) in sends:
        #if (time_ < time()):
    #    server.sendto(data,addr)
    #    sends.remove((data,addr))


def handler(rs,addr):
    try:
        packetnumber = rs.short()
        while not rs.at_end():
        
            byte = rs.byte()
            print str(byte) + " received from " + str(addr)        
            
            if addr[0] == str(usgn_address):
                status = rs.byte()
                if byte == 0x21:
                    if status == 2:
                        add("Server registered to usgn")
                    elif status== 1:
                        add("Couldn't register to usgn: already registered!")
                    else:
                        add("Couldnt register to usgn: "+ str(status))
                elif byte == 28:
                    if status == 2:
                        add("USGN status updated")
                    else:
                        add("USGN status unknown: " + str(status))
                elif byte == 29:
                    if status == 1:
                        add("Server removed from usgn")
                    else:
                        add("Server not removed from usgn?")
                else:
                    add("Unknown packet received from USGN: " + str(byte))
            
            else: # non usgn packed received
                
                if packetnumber %2 == 0:
                    ### yes i have received your message...
                    ws = WriteStreamCC()
                                        
                    ws.WriteByte(1)
                    ws.WriteShort(packetnumber)
                    
                    if addr in players:
                        sendone(ws.get(),addr,False,False)
                    else:
                        sendone(ws.get(),addr,False,True)
                    
                    
                    ###
                
                if byte == 3: # idk
                    num = rs.short()
                elif byte == 240: # recv team name
                    typ = rs.byte()
                    
                    pl = get_player(addr)
                    
                    if typ == 1:
                        len = rs.short()
                        txt = rs.string(len)                        
                        id = pl.id
                        onSay(0,txt)
                    elif typ == 20:
                        uk2 = rs.byte()
                        k3 = rs.byte()
                        teamname= rs.autostring()                    
                        pl.team = teamname                        
                        onUpdateTeam(pl.id,teamname)
                    
                elif byte == 251: # recv info
                    random_number = rs.int()
                    ws = WriteStreamCC()
                    ws.WriteByte(251);ws.WriteInt(random_number);ws.WriteString(sv_name);ws.WriteByte(sv_currentplayers)
                    ws.WriteByte(sv_maxplayers); ws.WriteByte(sv_gamemode)
                    sendone(ws.get(),addr,False,True)         
                elif byte == 252: # recv join
                    dunno1 = rs.short()
                    dunno2 = rs.short()
                    name = rs.autostring()
                    dunno3 = rs.int()
                    print name + " connected!"                    
                    
                    pl = Player(name,'',addr)
                    players.append(pl)
                    pl.id = players.index(pl)                  
                    sendone(gen_lobbycontent(addr).get(),addr,True)
                    
                    ### okay come in ############
                    ws2 = WriteStreamCC()
                    ws2.WriteByte(10)
                    sendone(ws2.get(),addr,False)
                    
                    sendone(gen_mapsize().get(),addr,True)
                    #############################
                    
    except IOError,e:
        print "IOError " + str(e)
                

def update(runtime):  
    pass  
   
def get_player(addr):
    for player in players:
        if player.address == addr:
            return player
    return None
    
def onSay(id,msg):
    ws = WriteStreamCC()
    ws.WriteByte(240) #msg id
    ws.WriteByte(1) # sub id
    ws.WriteByte(id)
    ws.WriteShort(len(msg))
    ws.WritePureString(msg)
    sendall(ws.get(),True)     

def onUpdateTeam(id,team_name):
    ws = WriteStreamCC()
    ws.WriteByte(240) #id
    ws.WriteByte(20) # sub id
    ws.WriteByte(id) #player id
    ws.WriteByte(1) # dunno
    ws.WriteByte(3) # dunno
    ws.WriteString(players[id].team)
    sendall(ws.get(),True)
    
    
def gen_lobbycontent(addr):
    ws = WriteStreamCC()
    pl = get_player(addr)
    ws.WriteByte(252)
    ws.WriteByte(2);
    ws.WriteByte(0);
    ws.WriteString(sv_name)
    ws.WriteByte(1); # his id?
    ws.WriteString(pl.name) # return his name back?
    ws.WriteInt(sv_player_health) # 64 0 0 0
    ws.WriteInt(sv_turn_time) # 2d 0 0 0
    ws.WriteInt(sv_backing_time) # 03 0 0 0
    
    ws.WriteInt(sv_supply_crates) #02 0 0 0
    ws.WriteInt(sv_supply_traps) # 0a 0 0 0
    ws.WriteInt(sv_friction) #? 02 0 0 0
    ws.WriteInt(sv_wind) #? 02 0 0 0
    ws.WriteInt(sv_fall_damage) #? 0 0 0 0
    ws.WriteInt(sv_mines) # 02 0 0 0
    ws.WriteInt(sv_mine_duds) # 0a 0 0 0
    ws.WriteInt(sv_explosion_delay) #03 0 0 0
    ws.WriteInt(sv_explosion_damage)  # 03 0 0 0
    ws.WriteInt(2)  #? 02 0 0 0
    ws.WriteInt(sv_bear_traps) #? 02 0 0 0
    ws.WriteInt(sv_medikits) # 01 0 0 0
    ws.WriteInt(sv_medikit_healing) # 19 0 0 0
    ws.WriteInt(sv_crates_on_map_limit) # 0f 00 00 00
    ws.WriteInt(sv_game_ender_start) #3 0 0 0
    ws.WriteInt(sv_game_ender_type) # 0 0 0 0
    ws.WriteInt(sv_gamemode) # 0 0 0 0
    ws.WriteInt(0)
    ws.WriteInt(0)
    ws.WriteString("Deathmatch")
    ws.WriteString("Flood")
    
    for i in players:
        print i
    
    
    for i in range(0,len(players)):
        dummy = players[i]
        ws.WriteByte(2)
        ws.WriteString(dummy.name)
        ws.WriteByte(0)
        if(dummy.team == None):
            ws.WriteByte(255);ws.WriteByte(1);
            ws.WriteByte(255);ws.WriteByte(0);
            ws.WriteByte(255);ws.WriteByte(0);
            ws.WriteByte(255);ws.WriteByte(0);
            ws.WriteByte(255);ws.WriteByte(0);
            ws.WriteByte(255);ws.WriteByte(3);
            ws.WriteByte(0); ws.WriteByte(0x0a)
        else:
            ws.WriteByte(0)#ws.WriteByte(4)        
            ws.WriteString(dummy.team)
            ws.WriteByte(0)
            ws.WriteByte(3)
            ws.WriteByte(0)
    
    return ws

def gen_mapsize():
    ws = WriteStreamCC()
    ws.WriteByte(241); ws.WriteByte(2)
    ws.WriteInt(sv_map_x); ws.WriteInt(sv_map_y)
    ws.WriteByte(4);ws.WriteByte(1); ws.WriteByte(0x80); ws.WriteByte(0)
    return ws

def quit():    
    ### deregister from usgn
    ws = WriteStreamCC()
    ws.WriteShort(1);ws.WriteByte(29); ws.WriteByte(2);
    #sendone    
    sends.append((ws.get(),(usgn_address,36963)))
    ###    
    server.close()
    
# function that sends a heartbeat to usgn.de every 60 seconds.    
def send_heartbeat():
    ws = WriteStreamCC()
    ws.WriteShort(1);ws.WriteByte(28);ws.WriteByte(2)
    sends.append((ws.get(),(usgn_address,36963)))    
    t = threading.Timer(60.0,send_heartbeat)
    t.start()
    

if __name__ == "__main__":
    start()
    atexit.register(quit)
    run()
