'''
Created on Dec 23, 2012

@author: Gebruiker
'''
from struct import pack,unpack

class WriteStreamCC:
    def __init__(self):
        self.data=""    
    def WriteShort(self,short):
        self.data += pack("H",short)
    def WriteInt(self,int):
        self.data += pack("i",int)
    def WriteFloat(self,float):
        self.data +=pack("f",float)
    def WriteByte(self,byte):
        if byte <0 : byte = 0
        if byte > 255: byte = 255
        self.data += chr(byte)
    def WriteLine(self,line):
        self.data += line + chr(13) + chr(10)
    def WriteString(self,string):
        self.WriteByte(len(string))
        self.data += string    
    def WritePureString(self,string):
        self.data += string
    def get(self):
        return self.data
    
    def __add__(self,other):
        b = WriteStreamCC()
        b.data = self.data + other.data
        return b

class ReadStreamCC:
    def __init__(self,data):
        self.data = data
        self.c = 0
        self.length = len(data)
        
    def at_end(self):
        return self.c == self.length
        
    def short(self):
        if self.length - self.c >= 2:
            r = unpack("H",self.data[self.c:self.c+2])
            self.c += 2
            return r[0]
        else:
            raise IOError("readshort out of bound!")
    def int(self):
        if self.length - self.c >= 4:
            r= unpack("i",self.data[self.c:self.c+4])
            self.c += 4
            return r[0]
        else:
            raise IOError("readint out of bound!")
    def long(self):
        if self.length - self.c >= 4:
            r = unpack("L",self.data[self.c:self.c+4])
            self.c += 4
            return r[0]
        else:
            raise IOError("readlong out of bounds!")
    def float(self):
        if self.length - self.c >= 4:
            r = unpack("f",self.data[self.c:self.c+4])
            self.c += 4
            return r[0]
        else:
            raise IOError("readfloat out of bounds!")
    def byte(self):
        if self.length - self.c >= 1:
            r = ord(self.data[self.c])
            self.c+=1
            return r
        else:
            raise IOError("readbyte out of bounds!")
    def line(self):
        line = self.data[self.c:].split(chr(130) + chr(10),1)[0]
        self.c += len(line) + 2
        return line
        
    def string(self,ln):
        if self.length - self.c >= ln:
            r = self.data[self.c:self.c+ln]
            self.c += ln
            return r
        else:
            raise IOError("readstring out of bounds!")
            
    def autostring(self):
        leng = self.byte()
        return self.string(leng)
        

def header(sh):
    return pack("H",sh)
            